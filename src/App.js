import React, { Component } from 'react';
import Header from './components/main-component/header';
import Sidebar from './components/main-component/sidebar';
import Footer from './components/main-component/footer';
import Content from './components/content';

class App extends Component {
  render() {
    return (
      <div> 
        <Header/> 
        <Sidebar/> 
        <Content/>
        <Footer/> 
      </div>
    );
  }
}

export default App;