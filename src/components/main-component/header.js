import React, { Component } from 'react';
import './header.css';

class Header extends Component {
    render() {
        return (
            <div>
                <header className="main-header">
                    {/* Logo */}
                    <a href="index2.html" className="logo">
                        {/* mini logo for sidebar mini 50x50 pixels */}
                        <span className="logo-mini"><b>PN</b>TR</span>
                        {/* logo for regular state and mobile devices */}
                        <span className="logo-lg"> <img src="assets/logo/logo-api.png" alt="" /></span>
                    </a>
                    {/* Header Navbar */}
                    <nav className="navbar navbar-static-top" role="navigation">

                        <a href="#" className="sidebar-toggle" data-toggle="push-menu" role="button">
                            <span className="sr-only">Toggle navigation</span>
                        </a>

                        <div className="navbar-left">
                            <h3 className="navbar-title">Dashboard</h3>
                        </div>



                        {/* Navbar Right Menu */}
                        <div className="navbar-custom-menu">
                            <ul className="nav navbar-nav">

                                <li className="dropdown user user-menu">
                                    {/* Menu Toggle Button */}
                                    <a href="link" className="dropdown-toggle" data-toggle="dropdown">
                                        {/* The user image in the navbar*/}
                                        <div className="user-container">


                                            <div>
                                                <h4>Ridwan Fadjar</h4><div className='text-right'>SALES</div>
                                            </div>
                                            <div className='user-image-container'>
                                                <img src="dist/img/user2-160x160.jpg" className="user-image" alt="UserImage" />
                                            </div>
                                        </div>


                                    </a>
                                    <ul className="dropdown-menu">
                                        {/* The user image in the menu */}
                                        <li className="user-header">
                                            <img src="dist/img/user2-160x160.jpg" className="img-circle" alt="UserImage" />
                                            <p>
                                                Alexander Pierce - Web Developer
                                                <small>Member since Nov. 2012</small>
                                            </p>
                                        </li>
                                        {/* Menu Body */}
                                        <li className="user-body">
                                            <div className="row">
                                                <div className="col-xs-4 text-center">
                                                    <a href="link">Followers</a>
                                                </div>
                                                <div className="col-xs-4 text-center">
                                                    <a href="link">Sales</a>
                                                </div>
                                                <div className="col-xs-4 text-center">
                                                    <a href="link">Friends</a>
                                                </div>
                                            </div>
                                            {/* /.row */}
                                        </li>
                                        {/* Menu Footer*/}
                                        <li className="user-footer">
                                            <div className="pull-left">
                                                <a href="link" className="btn btn-default btn-flat">Profile</a>
                                            </div>
                                            <div className="pull-right">
                                                <a href="link" className="btn btn-default btn-flat">Sign out</a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                {/* Control Sidebar Toggle Button */}

                            </ul>
                        </div>
                    </nav>
                </header>

            </div>
        );
    }
}

export default Header;