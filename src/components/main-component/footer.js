import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <div>
                <footer className="main-footer">
                    {/* To the right */}
                    <div className="pull-right hidden-xs">
                        <b>Version</b>1.0
                    </div>
                    {/* Default to the left */}
                    <strong>Copyright © 2019 <a  href="link">Akses Prima Indonesia</a>.</strong>  
                </footer>

            </div>
        );
    }
}

export default Footer;