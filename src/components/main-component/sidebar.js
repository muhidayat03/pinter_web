import React, { Component } from 'react';

class Sidebar extends Component {
    render() {
        return (
            <div>
                <aside className="main-sidebar">
                    {/* sidebar: style can be found in sidebar.less */}
                    <section className="sidebar">
                        <div className="border-top">

                        </div>
                        {/* Sidebar Menu */}
                        <ul className="sidebar-menu" data-widget="tree">
                             
                            <li><a  href="link"><i className="fa fa-dashboard" /> <span>Dashboard</span></a></li>
                            <li><a  href="link"><i className="fa fa-book" /> <span>Registrasi & AKtifasi</span></a></li>
                            <li><a  href="link"><i className="fa fa-dashboard" /> <span>Perubahan Layanan</span></a></li>
                            
                        </ul>
                        {/* /.sidebar-menu */}
                    </section>
                    {/* /.sidebar */}
                </aside>

            </div>
        );
    }
}

export default Sidebar;