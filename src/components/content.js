import React, { Component } from 'react';

class Content extends Component {
    render() {
        return (
            <div className="content-wrapper">
                {/* Content Header (Page header) */}
                <section className="content-header">

                </section>
                {/* Main content */}
                <section className="content container-fluid">
                    {
                        /*------------------------
                    | Your Page Content Here |
                ------------------------*/
                    }
                </section>
                {/* /.content */}
            </div>

        );
    }
}

export default Content;